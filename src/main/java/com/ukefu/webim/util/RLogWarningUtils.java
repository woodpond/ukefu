package com.ukefu.webim.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.web.model.ReqlogWarningAction;
import com.ukefu.webim.web.model.ReqlogWarningContent;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

/**
 * 	预警工具类
 *
 */
public class RLogWarningUtils {

	
	public static void getWarningContent(List<ReqlogWarningAction> reqlogWarningActionList, List<ReqlogWarningContent> reqlogWarningContentList ,StringBuffer str, StringBuffer trigStr){
		for(ReqlogWarningAction reqlogWarningAction : reqlogWarningActionList) {
			/**
			 * 遍历配置中的预警
			 */
			SysDic warnDic = UKeFuDic.getInstance().getDicItem(reqlogWarningAction.getWarnid());
			if (warnDic != null) {
				boolean warn = false;
				ReqlogWarningContent content = new ReqlogWarningContent();
				//正则匹配
				if (!StringUtils.isBlank(warnDic.getDescription()) && str != null && !StringUtils.isBlank(str.toString())) {
					String reg = warnDic.getDescription() ;
					if (!StringUtils.isBlank(reg)) {
						Pattern p = Pattern.compile(reg);
						Matcher m = p.matcher(str.toString());
						if (m.find()) {
							warn = true;
							content.setWarn(warnDic.getName());
							SysDic lvDic =  UKeFuDic.getInstance().getDicItem(warnDic.getParentid()) ;
							if (lvDic != null) {
								content.setWarnlv(lvDic.getName());
							}
						}
					}
				}
				//
				
				if (warn) {
					//拦截动作
					if (!StringUtils.isBlank(reqlogWarningAction.getActionid())) {
						String[] actions = reqlogWarningAction.getActionid().split(",");
						if (actions != null && actions.length > 0) {
							for(int i=0;i<actions.length;i++) {
								if (!StringUtils.isBlank(actions[i])) {
									SysDic actionDic = UKeFuDic.getInstance().getDicItem(actions[i]);
									content.setAction(i!=0?content.getAction()+","+actionDic.getName():actionDic.getName());
								}
							}
						}
					}
					reqlogWarningContentList.add(content);
					if (trigStr != null) {
						trigStr.append(!StringUtils.isBlank(trigStr.toString())?","+warnDic.getId():warnDic.getId());
					}
				}
			}
		}
	}
	
	/**
	 * 	根据用户id字符串得出用户email和电话号码
	 * @param touser
	 * @param useremailList
	 * @param usermobileList
	 */
	public static void getTouser(String touser,List<String> useremailList,List<String> usermobileList) {
		if (!StringUtils.isBlank(touser)) {
			@SuppressWarnings("unchecked")
			List<User> touserList = (List<User>)CacheHelper.getSystemCacheBean().getCacheObject(UKDataContext.SYSTEM_CACHE_WARNING_TOUSER, UKDataContext.SYSTEM_ORGI) ;
			if (touserList != null && touserList.size() > 0) {
				for(User utUser : touserList) {
					if (!StringUtils.isBlank(utUser.getEmail())) {
						useremailList.add(utUser.getEmail());
					}
					if (!StringUtils.isBlank(utUser.getMobile())) {
						usermobileList.add((utUser.getMobile()));
					}
				}
			}
		}
	}
}
