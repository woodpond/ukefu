package com.ukefu.webim.service.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.VoiceTranscription;

public abstract interface VoiceTranscriptionRepository
  extends ElasticsearchRepository<VoiceTranscription, String> ,VoiceTranscriptionESRepository
{
}
