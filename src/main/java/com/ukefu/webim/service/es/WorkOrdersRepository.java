package com.ukefu.webim.service.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.WorkOrders;

import java.util.List;

public interface WorkOrdersRepository extends  ElasticsearchRepository<WorkOrders, String> , WorkOrdersEsCommonRepository {

    public List<WorkOrders> findByOrdernoAndOrgi(String orderno , String orgi) ;

}
