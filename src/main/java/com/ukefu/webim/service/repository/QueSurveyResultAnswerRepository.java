package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.QueSurveyResultAnswer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public abstract interface QueSurveyResultAnswerRepository extends JpaRepository<QueSurveyResultAnswer, String>{

  public abstract Page<QueSurveyResultAnswer> findByResultidAndProcessidAndOrgi(String resultid,String processid,String orgi ,Pageable paramPageable);

  public abstract QueSurveyResultAnswer findByIdAndOrgi(String id, String orgi);
  
  public abstract List<QueSurveyResultAnswer> findByResultidAndProcessidAndOrgi(String resultid,String processid,String orgi);

  public abstract Page<QueSurveyResultAnswer> findAll(Specification<QueSurveyResultAnswer> spec, Pageable page) ;

  @Query(value = "SELECT count(id) as matchnum,answerid,answertype FROM uk_que_result_answer s WHERE 1=1 AND " +
          "IF (?1!='',s.createtime >= ?1,1=1) AND IF (?2!='',s.createtime <= ?2,1=1)  AND IF (?3!='',s.questionid = ?3,1=1) group by answerid  "
          ,nativeQuery = true)
  public abstract List<Object> countByQuestionid(Date dateStart, Date dateEnd, String questionid) ;

}
