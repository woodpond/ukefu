package com.ukefu.webim.service.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.Topic;

public interface XiaoETopiccRepository extends JpaRepository<Topic, String>{
	public abstract List<Topic> findByOrgi(String orgi);
}
